package main;

import deck.Deck;
import deck.Card;
import deck.Color;
import player.Player;
import java.util.Scanner;
import java.lang.NumberFormatException;

public class Main {
	public static void main(String[] args) {
		Deck frenchDeck = new Deck();
	    Card card;
	    Player p1 = new Player(15000);
	    Player computer = new Player(100000);

	    for(int i =0; i< 4; i++){
	      for(int j = 0; j< 13; j++){
	        Color color = Color.PICK;
	        String value = "A";
	        switch (i){
	          case 0:
	            color = Color.PICK;
	            break;
	          case 1:
	            color = Color.TREFF;
	            break;
	          case 2:
	            color = Color.KARO;
	            break;
	          case 3:
	            color = Color.KOR;
	            break;
	        }
	        switch (j){
	          case 0:
	            value = "A";
	            break;
	          case 1:
	          case 2:
	          case 3:
	          case 4:
	          case 5:
	          case 6:
	          case 7:
	          case 8:
	          case 9:
	            value = String.valueOf(j+1);
	            break;
	          case 10:
	            value = "J";
	            break;
	          case 11:
	            value = "K";
	            break;
	          case 12:
	            value = "Q";
	            break;
	        }
	        card = new Card(value, color);
	        frenchDeck.addCard(card);
	      }
	    }
	    
	    frenchDeck.shuffle();

	    Scanner sc = new Scanner(System.in);
	    String action;
	    int amount;
	    int bet = 500;
	    while(frenchDeck.sizeOfDeck() > 3){
	      System.out.println("Uj jatek");
	      bet = 1000;
	      computer.raise(500);
	      p1.raise(500);
	      p1.getCard(frenchDeck.draw());
	      p1.getCard(frenchDeck.draw());
	      computer.getCard(frenchDeck.draw());
	      System.out.println(p1.printCards());
	      System.out.println("Mit szeretnel csinalni: emeleshez ird be: raise; megallashoz : stop; kartya keresehez: getCard");
	      action = sc.nextLine();
	      while(!action.equals("stop") && p1.sumPlayerCards() < 22){
	        if(action.equals("getCard") && frenchDeck.sizeOfDeck() != 0){
	          p1.getCard(frenchDeck.draw());
	        }else if(action.equals("raise")){
	          System.out.println("Mennyivel szeretnel emelni?");
	          try{
	            amount = Integer.parseInt(sc.nextLine());
	            p1.raise(amount);
	            bet += amount;
	            computer.raise(amount);
	            bet += amount;
	          }catch(NumberFormatException e){
	            System.out.println("Nem sz�mot adt�l meg");
	          }
	        }
	        System.out.println(p1.printCards());
	        if(p1.sumPlayerCards() < 22){
	          System.out.println("Mit szeretnel csinalni: emeleshez ird be: raise; megallashoz : stop; kartya keresehez: getCard");
	          action = sc.nextLine();
	        }
	      }
	      if(p1.sumPlayerCards() > 21){
	        System.out.println(computer.printCards());
	        System.out.println("\nTul sok az ertek, igy a gep nyert\n");
	        computer.collectChips(bet);
	      }else{
	        while(computer.sumPlayerCards() < p1.sumPlayerCards()){
	          computer.getCard(frenchDeck.draw());
	        }
	        System.out.println(computer.printCards());
	        if(computer.sumPlayerCards() > p1.sumPlayerCards() && computer.sumPlayerCards() < 22){
	          computer.collectChips(bet);
	          System.out.println("\nGep nyert\n");
	        }else if(computer.sumPlayerCards() == p1.sumPlayerCards()){
	          computer.collectChips(bet/2);
	          p1.collectChips(bet/2);
	          System.out.println("\ndontetlen\n");
	        }else{
	          p1.collectChips(bet);
	          System.out.println("\nA jatekos nyert\n");
	        }
	      }
	      p1.removeCards();
	    }
	}
}
