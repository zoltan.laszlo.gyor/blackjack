package player;
import deck.Card;
import java.lang.NumberFormatException;
import java.util.ArrayList;

public class Player {
	private ArrayList<Card> playerCards;
	  private int chip;
	  private boolean inGame;

	  public Player(int chip){
	    this.playerCards = new ArrayList<>();
	    this.chip = chip;
	    this.inGame = true;
	  }

	  public int currentValue(){
	    return this.chip;
	  }

	  public void raise(int chip){
	    this.chip -= chip;
	  }

	  public void stop(){
	    this.inGame = false;
	  }

	  public void getCard(Card card){
	    this.playerCards.add(card);
	  }

	  public void collectChips(int chip){
	    this.chip += chip;
	  }

	  public void rejoinGame(){
	    this.inGame = true;
	  }

	  public int sumPlayerCards(){
	    int sum = 0;
	    int numberOfA = 0;
	    String currentValue;
	    for(int i = 0; i< playerCards.size();i++){
	      currentValue = playerCards.get(i).getValue();
	      if(currentValue.equals("A")){
	        numberOfA++;
	      }
	      else if(currentValue.equals("K") || currentValue.equals("Q") || currentValue.equals("J")){
	        sum += 10;
	      }else{
	        try{
	          sum += Integer.parseInt(currentValue);
	        }catch(NumberFormatException e){
	          System.out.println("ERROR");
	        }
	      }
	    }

	    for(int i = 0; i<numberOfA; i++){
	      if(sum + 11 > 21){
	        sum += 1;
	      }else{
	        sum += 11;
	      }
	    }
	    return sum;
	  }

	  public String printCards(){
	    StringBuilder sb = new StringBuilder();
	    for(int i = 0; i<playerCards.size();i++){
	      sb.append(playerCards.get(i).getColor());
	      sb.append(" ");
	      sb.append(playerCards.get(i).getValue());
	      sb.append(" ");
	    }
	    sb.append("PENZ: "+this.chip);
	    return sb.toString();
	  }

	  public void removeCards(){
	    this.playerCards = new ArrayList<>();
	    this.playerCards.clear();
	  }

}
