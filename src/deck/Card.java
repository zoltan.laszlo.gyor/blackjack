package deck;
import deck.Color;

public class Card{
  private String value;
  private Color color;

  public Card(String value, Color color){
    this.value = value;
    this.color = color;
  }

  public String getValue(){
    return this.value;
  }
  public Color getColor(){
    return this.color;
  }
}