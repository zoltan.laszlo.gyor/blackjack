package deck;
import deck.Card;
import deck.Color;
import java.util.Random;
import java.util.ArrayList;
import java.lang.StringBuilder;

public class Deck{
  private ArrayList<Card> cards;
  private int numberOfCards;

  public Deck(){
    this.cards = new ArrayList<>();
    this.numberOfCards = 0;
  }

  public void addCard(Card card){
    this.cards.add(card);
    this.numberOfCards += 1;
  }

  public Card draw(){
    Card drawnCard = cards.get(numberOfCards-1);
    cards.remove(numberOfCards-1);
    numberOfCards -= 1;
    return drawnCard;
  }

  public void shuffle(){
    ArrayList<Card> shuffledCards = new ArrayList<>();
    Random random = new Random();
    for(int i = 0; i<numberOfCards;i++){
      int j =  random.nextInt(cards.size());
      shuffledCards.add(cards.get(j));
      cards.remove(j);
    }
    cards = shuffledCards;
  }



  public int sizeOfDeck(){
    return cards.size();
  }
}